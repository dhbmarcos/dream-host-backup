#!/bin/bash
# Configure external backup

set -e

DOMAIN=""
DREAMHOST_USER=""
MYSQL_USER=""
MYSQL_PASSWORD=""
MYSQL_DATABASE=""
GIT_USER=""
GIT_PASSWORD=""
GIT_PATH_SERVER=""
GIT_NAME=""
GIT_MAIL=""

echo "Setting git access...";
git config --global user.name "${GIT_NAME}";
git config --global user.email "${GIT_MAIL}";
cd /home/${DREAMHOST_USER}/${DOMAIN};
git init;
git remote add origin https://${GIT_USER}:${GIT_PASSWORD}@${GIT_PATH_SERVER}.git;
echo "Done.";

echo "Setting database backup...";
mysqldump --add-drop-table -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} > database.backup.sql;
echo "Done.";

echo "Setting files backup...";
git add .;
git commit -m "Initial commit";
echo "Done.";

echo "Making backup...";
git push -u origin master;
echo "All done.";
