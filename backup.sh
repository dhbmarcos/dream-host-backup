#!/bin/bash
# Make external backup

set -e

DOMAIN=""
DREAMHOST_USER=""
MYSQL_USER=""
MYSQL_PASSWORD=""
MYSQL_DATABASE=""

echo "Setting database backup...";
cd /home/${DREAMHOST_USER}/${DOMAIN};
mysqldump --add-drop-table -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} > database.backup.sql;
echo "Done.";

echo "Setting files backup...";
git add -A;
git commit -m "Daily backup";
echo "Done.";

echo "Making backup...";
git push -u origin master;
rm -v database.backup.sql;
echo "All done.";
