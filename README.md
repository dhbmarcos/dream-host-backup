# DreamHost

Process to automate Dream Host Sites.


| Parameter              | Description                                                 |
| ---------------------- | ----------------------------------------------------------- |
| **${DOMAIN}**          | Internet domain. *"example.com"*                            |
| **${DREAMHOST_USER}**  | DreamHost user for domain. *"user_example_com"*             |
| **${MYSQL_USER}**      | Database username. *"example_user"*                         |
| **${MYSQL_PASSWORD}**  | Database password. *"my strong password"*                   |
| **${MYSQL_DATABASE}**  | Database name. *"example"*                                  |
| **${GIT_USER}**        | Git server user account. *"user"*                           |
| **${GIT_PASSWORD}**    | Git server password account. *"my strong password"*         |
| **${GIT_PATH_SERVER}** | Git URL in Git domain. *"mygitserver.com/user/example.git"* |
| **${GIT_NAME}**        | Git server user name *"User"*                               |
| **${GIT_MAIL}**        | Git server user e-mail *"user@example.com"*                 |

~~~bash
DOMAIN="${DOMAIN}"
DREAMHOST_USER="${DREAMHOST_USER}"
MYSQL_USER="${MYSQL_USER}"
MYSQL_PASSWORD="${MYSQL_PASSWORD}"
MYSQL_DATABASE="${MYSQL_DATABASE}"
GIT_USER="${GIT_USER}"
GIT_PASSWORD="${GIT_PASSWORD}"
GIT_PATH_SERVER="${GIT_PATH_SERVER}"
GIT_NAME="${GIT_NAME}"
GIT_MAIL="${GIT_MAIL}"
~~~


# Configure external backup

~~~bash
git config --global user.name "${GIT_NAME}"
git config --global user.email "${GIT_MAIL}"
cd /home/${DREAMHOST_USER}/${DOMAIN}
git init
git remote add origin https://${GIT_USER}:${GIT_PASSWORD}@${GIT_PATH_SERVER}.git
mysqldump --add-drop-table -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} > database.backup.sql
git add .
git commit -m "Initial commit"
git push -u origin master
~~~



# Backup all

~~~bash
cd /home/${DREAMHOST_USER}/${DOMAIN}
mysqldump --add-drop-table -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} > database.backup.sql
git add -A
git commit -m "Daily backup"
git push -u origin master
rm -v database.backup.sql
~~~


# Restore all

~~~bash
cd /home/${DREAMHOST_USER}/${DOMAIN}
rm -Rf *
git reset --hard
mysql -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -e "drop database if exists ${MYSQL_DATABASE}; create database ${MYSQL_DATABASE}"
mysql -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} < database.backup.sql
rm -v database.backup.sql
~~~


# Backup database

~~~bash
mysqldump --add-drop-table -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} > database.backup.sql
~~~


# Restore database

~~~bash
mysql -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -e "drop database if exists ${MYSQL_DATABASE}; create database ${MYSQL_DATABASE}"
mysql -h mysql.${DOMAIN} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} < database.backup.sql
~~~


# Setting cron job to daily backup

1. In SSH DreamHost interface, copy file **backup.sh** and paste in **/home/${DREAMHOST_USER}/${DOMAIN}.backup.sh**;

   ~~~bash
   cd /home/${DREAMHOST_USER};
   wget -c https://gitlab.com/dhbmarcos/dreamhost/-/raw/master/backup.sh;
   mv backup.sh ${DOMAIN}.backup.sh;
   chmod +x ${DOMAIN}.backup.sh;
   ~~~

2. Configure parameters in **${DOMAIN}.backup.sh** file;
3. In DreamHost panel, access **More**, **Cron Jobs**;
4. Create new cron job through button **Add New Cron Job**;
5. Select domain user through selection box **User**;
6. In **Tittle** text box, put `${DOMAIN} daily backup`;
7. In **Email output to** text box, put `${GIT_MAIL}`;
7. In **Command to run** text box, put `/home/${DREAMHOST_USER}/${DOMAIN}.backup.sh;`;
8. Enable **Use locking** option;
9. Select ***Daily*** in **When to run** selection box.
10. Confirm through button **Add**.


# License

[MIT](LICENSE)
